import extensions.CSVFile;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
class Projet extends Program {
    final String RESET = "\u001B[0m";
    final String BLACK = "\u001B[30m";
    final String RED = "\u001B[31m";
    final String GREEN = "\u001B[32m";
    final String YELLOW = "\u001B[33m";
    final String BLUE = "\u001B[34m";
    final String PURPLE = "\u001B[35m";
    final String CYAN = "\u001B[36m";
    final String WHITE = "\u001B[37m";
    final String chemin = "../ressources/sauvegarde.csv"; //Chemin du fichier de sauvagarde
    final String cheminQuestion = "../ressources/questions.csv"; //Chemin du fichier de question
    ////////////
    //ACCUEIL//
    ///////////
    //Affichage de l'écran d'accueil
    void accueil(){
        refresh();
        affichageAccueil();
        //Détecte si l'on veut commencer à jouer ou reset la partie
        saisieAccueil();
        //Fin du programme accueil, on reset l'affichage
        refresh();
    }
    //Saisie d'un input pour l'accueil (sans que l'écran se décale à cause de spam)
    void saisieAccueil(){
        //Déclaration des variables
        String input = "xx";
        //Analyse de la saisie
        while(length(input)>1){
            input = majuscule(readString());
            refresh();
            affichageAccueil();
            if(equals(input,"X")){
                resetSauvegarde();
                accueil();
            }
        }
    }
    //////////////////
    //GESTION DU JEU//
    //////////////////
    //Reset de la sauvegarde
    void resetSauvegarde(){
        //Déclaration des variables
        CSVFile sauvegarde = loadCSV(chemin);
        //Reset de la sauvegarde
        String[][] tab = new String[][]{{"Point de Sauvegarde","0"},
        {"Date de Sauvegarde","(XX/XX/XXXX XX:XX)"},
        {"Prenom","Inconnue"},
        {"Niveau","0"},
        {"PV","0"},
        {"PV max","0"},
        {"Mana","0"},
        {"Mana Max","0"},
        {"Monstres vaincus","0"}};
        saveCSV(tab, chemin);
        accueil();
    }
    //Refresh de l'écran
    void refresh(){
        clearScreen();
        cursor(1,1);
    }
    //Sauvegarde de la partie et des stats
    void sauvegarde(Personnage p, int nbrMonstresVaincus){
        //Déclaration des variables
        DateFormat format = new SimpleDateFormat("(dd/MM/yyyy HH:mm)");
        Date date = new Date();
        //Reset de la sauvegarde
        String[][] tab = new String[][]{{"Point de Sauvegarde"," "},
        {"Date de Sauvegarde"," "},
        {"Prenom"," "},
        {"Niveau"," "},
        {"PV"," "},
        {"PV max"," "},
        {"Mana"," "},
        {"Mana Max"," "},
        {"Monstres vaincus"," "}};
        tab[0][1] = String.valueOf(p.sauvegarde);
        String dateAuj = format.format(date);
        tab[1][1] = dateAuj;
        tab[2][1] = p.prenom;
        tab[3][1] = String.valueOf(p.stats.niveau);
        tab[4][1] = String.valueOf(p.stats.pv);
        tab[5][1] = String.valueOf(p.stats.pvMax);
        tab[6][1] = String.valueOf(p.stats.mana);   
        tab[7][1] = String.valueOf(p.stats.manaMax);
        tab[8][1] = String.valueOf(nbrMonstresVaincus);
        saveCSV(tab, chemin);
    }
    //Récupération des données de sauvegarde du joueur
    Personnage donneeJoueur(Personnage p){
        //Déclaration des variables
        p.stats = new Stats();
        CSVFile sauvegarde = loadCSV(chemin);
        //Récupération des données de sauvegarde du joueur
        p.sauvegarde = Integer.parseInt(getCell(sauvegarde, 0, 1));
        p.prenom = getCell(sauvegarde, 2, 1);
        p.stats.niveau = Integer.parseInt(getCell(sauvegarde, 3, 1));
        p.stats.pv = Integer.parseInt(getCell(sauvegarde, 4, 1));
        p.stats.pvMax = Integer.parseInt(getCell(sauvegarde, 5, 1));
        p.stats.mana = Integer.parseInt(getCell(sauvegarde, 6, 1));
        p.stats.manaMax = Integer.parseInt(getCell(sauvegarde, 7, 1));
        return p;
    }
    //Récupération des données de sauvegarde des monstres
    int donneeMonstre(){
        CSVFile sauvegarde = loadCSV(chemin);
        return Integer.parseInt(getCell(sauvegarde, 8, 1));
    }
    //Passe un String en Majuscule
    void testMajuscule(){
        assertEquals("SALUT",majuscule("SALUT"));
        assertEquals("SALUT",majuscule("salut"));
        assertEquals("SALUT",majuscule("SaLuT"));
    }
    String majuscule(String mot){
        //Déclaration des variablesl
        String resultat = "";
        char lettre = ' ';
        //Convertit les minuscules en majuscules (en vérifiant que c'est bien des lettres)
        for(int i = 0; i<length(mot); i++){
            if(charAt(mot,i)>=97 && charAt(mot,i)<=122){
                lettre = (char) (charAt(mot,i)-32);

            }else{
                lettre = charAt(mot,i);
            }
            resultat = resultat + "" + lettre;
        }
        return resultat;
    }
    //Vérifie si la partie n'est pas fini
    void testFinDuJeu(){
        assertTrue(finDuJeu(0));
        int nombre = (int) (random()*100+1);
        assertFalse(finDuJeu(nombre));
    }
    boolean finDuJeu(int nbrMonstresVaincus){
        if(nbrMonstresVaincus==0){
            return true;
        }else{
            return false;
        }
    }
    ////////////////
    //INTRODUCTION//
    ////////////////
    void introduction(){
        //Déclaration des variables
        Personnage p = new Personnage();
        Stats stats = new Stats();
        //Début de l'intro
        sceneChateau();
        dialogue1();
        p = creationPerso(); //Demande le prénom de l'utilisateur
        sauvegarde(p,0);
        sceneChateau();
        dialogue2();
        readString();
    }
    Personnage creationPerso(){
        //Déclaration des variables
        Personnage p = new Personnage();
        p.stats = new Stats();
        //Début de la création du personnage
        boolean fini = false;
        while(fini==false){
            println("Comment vous appelez vous ? (moins de 10 caractères)");
            p.prenom = readString();
            if(length(p.prenom)>10 || length(p.prenom)==0){
                refresh();
                sceneChateau();
                dialogue1();
            }else{
                fini = true;
            }
        }
        p.sauvegarde = 0;
        p.stats.niveau = 1;
        p.stats.pv = 20; p.stats.pvMax = 20;
        p.stats.mana = 10;p.stats.manaMax = 10;
        return p;
    }
    ///////////
    //PLATEAU//
    ///////////
    //Génération du plateau de façon aléatoire est paramétrable
    Plateau generationPlateau(int hauteur, int largeur, int nbrMonstres){
        //Déclaration des variables
        Plateau plateau = new Plateau();
        plateau.cases = new Cases[hauteur][largeur];
        int x = 0;
        int z = 0;
        int monstres = 0;
        int rocher = 0;
        boolean okRocher = true;
        //Génération des bordures de la map
        for(int i=0; i<hauteur; i++){
            for(int u=0; u<largeur; u++){
                if((i==0 && u%2==0) || u==0 || (i==(length(plateau.cases,1)-1) && u%2==0) || u==(length(plateau.cases,2)-1) ){
                    plateau.cases[i][u] = generationCases(true);
                }else{
                    plateau.cases[i][u] = generationCases(false);
                }
            }
        }
        //Positionne le joueur à un point fixe sur la map
        plateau.cases[20][50].joueur = true;
        //Positionne aléatoirement 5 monstres sur la map
        while(monstres!=nbrMonstres){
            x = (int) (random()*(hauteur-4)+2);
            z = (int) (random()*(largeur-4)+2);
            if(plateau.cases[x][z].obstacle==false && plateau.cases[x][z].monstres==false && plateau.cases[x][z].joueur==false){
                monstres = monstres + 1;
                plateau.cases[x][z].monstres=true;
            }
        }
        //Positionne aléatoirement 15 rocher sur la map sur la map
        while(rocher!=15){
            okRocher = true;
            x = (int) (random()*(hauteur-4)+2);
            z = (int) (random()*(largeur-4)+2);
            for(int i=(x-2); i<=(x+2); i++){
                for(int u=(z-2); u<=(z+2); u++){
                    if(plateau.cases[i][u].obstacle==true || plateau.cases[i][u].monstres==true || plateau.cases[i][u].joueur==true){
                        okRocher = false;
                    }
                }
            }
            if(okRocher==true){
                for(int i=(x-1); i<=(x+1); i++){
                    for(int u=(z-1); u<=(z+1); u++){
                        plateau.cases[i][u].rocher=true;
                    }
                }
                rocher = rocher + 1;
            }
        }
        return plateau;
    }
    //Génération cases
    Cases generationCases(boolean obstacle){
        Cases cases = new Cases();
        cases.obstacle = obstacle;
        cases.monstres = false;
        cases.joueur = false;
        cases.rocher = false;
        return cases;
    }
    //Transcription du Plateau en String
    void plateauToString(Plateau plateau){
        refresh();
        for(int i=0; i<length(plateau.cases,1); i++){
            for(int u=0; u<length(plateau.cases,2); u++){
                if(plateau.cases[i][u].obstacle==true){
                    print("■");
                }else if(plateau.cases[i][u].joueur==true){
                    print(GREEN+"⬤"+RESET);
                }else if(plateau.cases[i][u].monstres==true){
                    print(RED+"⬤"+RESET);
                }else if(plateau.cases[i][u].rocher==true){
                    print(CYAN+"■"+RESET);
                }else{
                    print(" ");
                }
            }
            println();
        }
    }
    //Actualise le tableau
    Plateau nouveauPlateau(Plateau plateau, char input){
        //Déclaration des variables
        int x = 0;
        int z = 0;
        //Supprime (visuellement) l'ancienne  position du joueur
        for(int i=0; i<length(plateau.cases,1); i++){
            for(int u=0; u<length(plateau.cases,2); u++){
                if(plateau.cases[i][u].joueur==true){
                    plateau.cases[i][u].joueur = false;
                    x = i;
                    z = u;
                }
            }
        }
        //Actualise la position du joueur en fonction de l'action
        if(input=='Z' && (x-1)!=0 && plateau.cases[(x-1)][z].rocher==false){
            x = x - 1;
        }else if(input=='Q' && (z-1)!=0 && plateau.cases[x][(z-1)].rocher==false){
            z = z - 1;
        }else if(input=='S' && (x+1)!=(length(plateau.cases,1)-1) && plateau.cases[(x+1)][z].rocher==false){
            x = x + 1;
        }else if(input=='D' && (z+1)!=(length(plateau.cases,2)-1) && plateau.cases[x][(z+1)].rocher==false){
            z = z + 1;
        }
        plateau.cases[x][z].joueur = true;
        return plateau;
    }
    ////////////
    //TUTORIEL//
    ////////////
    void tutoriel1(Plateau plateau, int nbrMonstresVaincus){
        //Déclaration des variables
        Personnage p = new Personnage();
        p.stats = new Stats(); 
        p = donneeJoueur(p);
        int pvMonstre = 16;
        char input = 'x';
        //Exécution du tutoriel
        plateauToString(plateau);
        //Tuto déplacement
        tutoDeplacement();
        readString(); //A CHANGER
        //Le joueur peut se déplacer jusqu'à ce qu'il déclenche un combat
        rechercheCombat(plateau);
        //Tuto combat partie 1
        refresh();
        ecranCombat();
        infoJoueur(pvMonstre);
        tuto1();
        readString();
        //Tuto combat partie 2
        refresh();
        ecranCombat();
        infoJoueur(pvMonstre);
        tuto2();
        readString();
        //Tuto combat partie 3
        refresh();
        ecranCombat();
        infoJoueur(pvMonstre);
        tuto3();
        readString();
        //Début réel du combat
        if(combat(nbrMonstresVaincus)==false){
            perdu();
        }
        //Soigne le joueur
        p.stats.pv = p.stats.pvMax; p.stats.mana = p.stats.manaMax;      
    }
    void tutoriel2(Plateau plateau){
        refresh();
        sceneChateau();
        finTuto();
        readString();
    }
    ///////////
    //FENETRE//
    ///////////
    void affichageAccueil(){
        //Déclaration des variables
        CSVFile sauvegarde = loadCSV(chemin);
        String dateSauvegarde = getCell(sauvegarde, 1, 1);
        //Accueil
        println("■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■"+RED+"                       ■ ■ ■   ■ ■ ■   ■   ■   ■ ■ ■     ■     ■       ■ ■ ■                       "+RESET+"■");
        println("■"+RED+"                       ■   ■     ■      ■ ■      ■     ■   ■   ■       ■                           "+RESET+"■");
        println("■"+RED+"                       ■ ■ ■     ■       ■       ■     ■ ■ ■   ■       ■ ■                         "+RESET+"■");
        println("■"+RED+"                       ■         ■      ■ ■      ■     ■   ■   ■       ■                           "+RESET+"■");
        println("■"+RED+"                       ■       ■ ■ ■   ■   ■     ■     ■   ■   ■ ■ ■   ■ ■ ■                       "+RESET+"■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■"+RED+"                               Appuyez sur une touche pour commencer                               "+RESET+"■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■"+CYAN+"                               Dernière sauvegarde "+dateSauvegarde+"                              "+RESET+"■");
        println("■"+GREEN+"                           (Appuyez sur X pour effacer votre sauvegarde)                           "+RESET+"■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■"+PURPLE+"     O                                                                                             "+RESET+"■");
        println("■"+PURPLE+"    ―|―                                                                                            "+RESET+"■");
        println("■"+PURPLE+"     |                                                                                             "+RESET+"■");
        println("■"+PURPLE+"    / \\    © BERNARD Quentin  &  MACZENKO Noé                                                      "+RESET+"■");
        println("■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■");
    }
    void perdu(){
        refresh();
        println(RED+"■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■"+RESET);
        println(RED+"■"+RESET+"                                                                                                   "+RED+"■"+RESET);
        println(RED+"■"+RESET+"                                   "+GREEN+"____   ____  ___ ___    ___"+RESET+"                                     "+RED+"■"+RESET);
        println(RED+"■"+RESET+"                                  "+GREEN+"/    | /    ||   |   |  /  _]"+RESET+"                                    "+RED+"■"+RESET);
        println(RED+"■"+RESET+"                                 "+GREEN+"|   __||  o  || _   _ | /  [_  "+RESET+"                                   "+RED+"■"+RESET);
        println(RED+"■"+RESET+"                                 "+GREEN+"|  |  ||     ||  \\_/  ||    _]  "+RESET+"                                  "+RED+"■"+RESET);
        println(RED+"■"+RESET+"                                 "+GREEN+"|  |_ ||  _  ||   |   ||   [_    "+RESET+"                                 "+RED+"■"+RESET);
        println(RED+"■"+RESET+"                                 "+GREEN+"|     ||  |  ||   |   ||     |"+RESET+"                                    "+RED+"■"+RESET);
        println(RED+"■"+RESET+"                                 "+GREEN+"|___,_||__|__||___|___||_____|"+RESET+"                                    "+RED+"■"+RESET);
        println(RED+"■"+RESET+"                                                                                                   "+RED+"■"+RESET);
        println(RED+"■"+RESET+"                                                                                                   "+RED+"■"+RESET);
        println(RED+"■"+RESET+"                                                                                                   "+RED+"■"+RESET);
        println(RED+"■"+RESET+"                                                                                                   "+RED+"■"+RESET);
        println(RED+"■"+RESET+"                                                                                                   "+RED+"■"+RESET);
        println(RED+"■"+RESET+"                        ,____                                        ____,                         "+RED+"■"+RESET);
        println(RED+"■"+RESET+"                        |---.\\    "+GREEN+" ___   __ __    ___  ____"+RESET+"         /.---|                         "+RED+"■"+RESET);
        println(RED+"■"+RESET+"                ___     |    `    "+GREEN+"/   \\ |  |  |  /  _]|    \\"+RESET+"        `    |     ___                 "+RED+"■"+RESET);
        println(RED+"■"+RESET+"               / .-\\  ./=)       "+GREEN+"|     ||  |  | /  [_ |  D  )"+RESET+"           (=\\.  /-. \\                "+RED+"■"+RESET);
        println(RED+"■"+RESET+"              |  | |_/\\/|        "+GREEN+"|  O  ||  |  ||    _]|    /"+RESET+"             |\\/\\_| |  |               "+RED+"■"+RESET);
        println(RED+"■"+RESET+"              ;  |-;| /_|        "+GREEN+"|     ||  :  ||   [_ |    \\"+RESET+"             |_\\ |;-|  ;               "+RED+"■"+RESET);
        println(RED+"■"+RESET+"             / \\_| |/ \\ |        "+GREEN+"|     | \\   / |     ||  .  \\"+RESET+"            | / \\| |_/ \\              "+RED+"■"+RESET);
        println(RED+"■"+RESET+"            /      \\/\\( |         "+GREEN+"\\___/   \\_/  |_____||__|\\_|"+RESET+"            | )/\\/      \\             "+RED+"■"+RESET);
        println(RED+"■"+RESET+"            |   /  |` ) |                                                | ( '|  \\   |             "+RED+"■"+RESET);
        println(RED+"■"+RESET+"            /   \\ _/    |                                                |    \\_ /   \\             "+RED+"■"+RESET);
        println(RED+"■"+RESET+"           /--._/  \\    |                                                |    /  \\_.--\\            "+RED+"■"+RESET);
        println(RED+"■"+RESET+"           `/|)    |    /                                                \\    |    (|\\`            "+RED+"■"+RESET);
        println(RED+"■"+RESET+"             /     |   |                                                  |   |     \\              "+RED+"■"+RESET);
        println(RED+"■"+RESET+"           .'      |   |                                                  |   |      '.            "+RED+"■"+RESET);
        println(RED+"■"+RESET+"          /         \\  |                                                  |  /         \\           "+RED+"■"+RESET);
        println(RED+"■"+RESET+"         (_.-.__.__./  /                                                  \\  \\.__.__.-._)          "+RED+"■"+RESET);
        println(RED+"■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■"+RESET);
        readString();
        algorithm();
    }
    void sceneChateau(){
        refresh();
        println("■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■");
        println("■                                                                                                   ■");
        println("■                                                    o                                              ■");
        println("■                                                "+RED+"_---"+RESET+"|         _ _ _ _ _                            ■");
        println("■                                             o   "+RED+"---"+RESET+"|     o   ]-I-I-I-[                            ■");
        println("■                            _ _ _ _ _ _  "+RED+"_---"+RESET+"|      | "+RED+"_---"+RESET+"|    \\ ` ' /                             ■");
        println("■                            ]-I-I-I-I-[   "+RED+"---"+RESET+"|      |  "+RED+"---"+RESET+"|    |.   |                              ■");
        println("■                             \\ `   '_/       |     / \\    |    | /^\\|                              ■");
        println("■                              [*]  __|       ^    / ^ \\   ^    | |*||                              ■");
        println("■                              |__   ,|      / \\  /    `\\ / \\   | ===|                              ■");
        println("■                           ___| ___ ,|__   /    /=_=_=_=\\   \\  |,  _|                              ■");
        println("■                           I_I__I_I__I_I  (====(_________)___|_|____|____                          ■");
        println("■                           \\-\\--|-|--/-/  |     I  [ ]__I I_I__|____I_I_|                          ■");
        println("■                            |[]      '|   | []  |`__  . [  \\-\\--|-|--/-/                           ■");
        println("■                            |.   | |' |___|_____I___|___I___|---------|                            ■");
        println("■                           / \\| []   .|_|-|_|-|-|_|-|_|-|_|-| []   [] |                            ■");
        println("■                          <===>  |   .|-=-=-=-=-=-=-=-=-=-=-|   |    / \\                           ■");
        println("■                          ] []|`   [] ||.|.|.|.|.|.|.|.|.|.||-      <===>                          ■");
        println("■                          ] []| ` |   |/////////\\\\\\\\\\\\\\\\\\.||__.  | |[] [                           ■");
        println("■                          <===>     ' ||||| |   |   | ||||.||  []   <===>                          ■");
        println("■                           \\T/  | |-- ||||| | O | O | ||||.|| . |'   \\T/                           ■");
        println("■                            |      . _||||| |   |   | ||||.|| |     | |                            ■");
        println("■                         ../|' v . | .|||||/____|____\\|||| /|. . | . ./                            ■");
        println("■                         .|//\\............/...........\\........../../\\\\\\                           ■");
        println("■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■");
    }
    void ecranCombat(){
        println("■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■");
        println("■                                                                                                   ■");
        println("■                                                              .-.                                  ■");
        println("■              "+RED+"_,."+RESET+"                                            (o.o)                                 ■");
        println("■            "+RED+",` -.)"+RESET+"                                            |=|                                  ■");
        println("■           "+RED+"( "+RESET+"_/-\\\\-._                                        __|__                                 ■");
        println("■          "+RED+"/,"+RESET+"|`--._,-^|           ,                         //.=|=.\\\\                   .-.         ■");
        println("■          "+RED+"\\_"+RESET+"| |`-._/||         ,'|                        // .=|=. \\\\                 (o.o)        ■");
        println("■            |  `-, / |        /  /                        \\\\ .=|=. //                  |=|         ■");
        println("■            |     || |       /  /                          \\\\(_=_)//                  __|__        ■");
        println("■         __,-<_     )`-/  `./  /                            (:| |:)                 //.=|=.\\\\      ■");
        println("■        '  \\   `---'   \\   /  /                              || ||                 // .=|=. \\\\     ■");
        println("■            |           |./  /                               () ()                 \\\\ .=|=. //     ■");
        println("■            /           //  /                                || ||                  \\\\(_=_)//      ■");
        println("■        \\_/' \\         |/  /                                 || ||                   (:| |:)       ■");
        println("■         |    |   _,^-'/  /                                 ==' '==                   || ||        ■");
        println("■         |    , ``  (\\/  /_                                                           () ()        ■");
        println("■          \\,.->._    \\X-=/^                                                           || ||        ■");
        println("■          (  /   `-._//^`                                                             || ||        ■");
        println("■           `Y-.____(__}                                                              ==' '==       ■");
        println("■            |   {__)                                                                               ■");
        println("■                  ()                                                                               ■");
        println("■                                                                                                   ■");
    }
    void fin(){
        println("■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■"+RED+"       ____   ____   _____     ______   _________     ___     _____   _______      ________        "+RESET+"■");
        println("■"+RED+"      |_  _| |_  _| |_   _|  .' ___  | |  _   _  |  .'   `.  |_   _| |_   __ \\    |_   __  |       "+RESET+"■");
        println("■"+RED+"        \\ \\   / /     | |   / .'   \\_| |_/ | | \\_| /  .-.  \\   | |     | |__) |     | |_ \\_|       "+RESET+"■");
        println("■"+RED+"         \\ \\ / /      | |   | |            | |     | |   | |   | |     |  __ /      |  _| _        "+RESET+"■");
        println("■"+RED+"          \\ ' /      _| |_  \\ `.___.'\\    _| |_    \\  `-'  /  _| |_   _| |  \\ \\_   _| |__/ |       "+RESET+"■");
        println("■"+RED+"           \\_/      |_____|  `.____ .'   |_____|    `.___.'  |_____| |____| |___| |________|       "+RESET+"■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■                                   "+GREEN+"Merci d'avoir joué à notre jeu                                  "+RESET+"■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■         "+BLUE+"Si vous voulez retentez l'aventure, il vous suffit d'appuyer sur X à l'écran titre        "+RESET+"■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■"+PURPLE+"     O                                                                                             "+RESET+"■");
        println("■"+PURPLE+"    ―|―                                                                                            "+RESET+"■");
        println("■"+PURPLE+"     |                                                                                             "+RESET+"■");
        println("■"+PURPLE+"    / \\    © BERNARD Quentin  &  MACZENKO Noé                                                      "+RESET+"■");
        println("■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■");

    }
    /////////////
    //Dialogues//
    /////////////
    void zoneVide(){
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■                                                                                                   ■");
        println("■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■");
    }
    void dialogue1(){
        println("■                                                                                                   ■");
        println("■  "+BLUE+"Mes humbles salutations voyageurs, bievenue dans le monde de "+RED+"PIXTALE"+BLUE+" !!"+RESET+"                          ■");
        println("■  "+RED+"PIXTALE"+BLUE+" est un monde où magie et féérie y règne !!"+RESET+"                                               ■");
        println("■  "+BLUE+"Mais avant de commencer votre périple, pourriez-vous vous présenter à moi ?"+RESET+"                      ■");
        println("■                                                                                                   ■");
        println("■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■");  
    }
    void dialogue2(){
        println("■                                                                                                   ■");
        println("■  "+BLUE+"Maintenant que je connais votre prénom cher voyageur, nous allons pouvoir "+RED+"débuter votre voyage"+BLUE+"."+RESET+"  ■");
        println("■  "+BLUE+"Pour débuter ce périple, je vais commencer par "+RED+"vous expliquer les fondamentale de ce monde"+BLUE+"."+RESET+"      ■");
        println("■  "+BLUE+"J'espère que vous êtes paré pour le voyage !!"+RESET+"                                                    ■");
        println("■                                                                                                   ■");
        println("■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■");
    }
    void tutoDeplacement(){
        println("■                                                                                                   ■");
        println("■  Vous pouvez \""+GREEN+"⬤"+RESET+"\" vous déplacer avec les touches "+RED+"ZQSD"+RESET+" en validant (avec entrée) chaque action.     ■");
        println("■  Vous ne pouvez pas passer à travers les \"■\" et les \""+CYAN+"■"+RESET+"\".                                          ■");
        println("■  Lorsque vous rentrez en conctact avec un \""+RED+"⬤"+RESET+"\", vous débutez un combat.                            ■");
        println("■                                                                                                   ■");
        println("■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■");
    }
    void tuto1(){
        println("■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■");
        println("■                                                                                                   ■");
        println("■  Vous avez débuté un combat. Pour rapidement vous expliquer, vous avez "+RED+"2 choix "+RESET+"lors d'un combat.  ■");
        println("■  Soit vous décidez "+RED+"d'attaquer directement"+RESET+", soit vous "+BLUE+"demandez de l'aide"+RESET+".                          ■");
        println("■  Pour "+RED+"réussir une attaque"+RESET+", il vous faut "+GREEN+"répondre bon à la question qui suivra"+RESET+".                    ■");
        println("■                                                                                                   ■");
        println("■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■");
    }
    void tuto2(){
        println("■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■");
        println("■                                                                                                   ■");
        println("■  Si cependant vous "+BLUE+"demandez de l'aide"+RESET+" vous aurez "+BLUE+"2 autres choix"+RESET+" à faire:                          ■");
        println("■  - Soit vous décidez de "+PURPLE+"vous soigner "+RESET+"contre 5 pts de mana.                                        ■");
        println("■  - Soit vous décidez de "+PURPLE+"d'avoir une mauvaise réponse en moins au QCM"+RESET+" contre 5 pts de mana.        ■");
        println("■                                                                                                   ■");
        println("■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■");
    }
    void tuto3(){
        println("■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■");
        println("■                                                                                                   ■");
        println("■  Ainsi, si vous "+RED+"attaquez"+RESET+" et que vous "+RED+"répondez bon"+RESET+", vous infligerez des dégats aux monstres.       ■");
        println("■  Si vous "+RED+"répondez mal"+RESET+", c'est lui qui vous fera des dégats, et vous perdez lorsque vos PV sont à 0 ■");
        println("■  Si vous choisissez de "+BLUE+"l'aide"+RESET+", votre aide sera appliquée et vous pourrez attaquer dans la volée.  ■");
        println("■                                                                                                   ■");
        println("■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■");
    }
    void finTuto(){
        println("■                                                                                                   ■");
        println("■  Bien joué cher aventurier, vous avez maintenant "+BLUE+"assimilé toutes les spécificités de notre monde"+RESET+". ■");
        println("■  Maintenant, je peux vous demander de vous balader dans notre monde pour "+RED+"défaire les 5 créatures"+RESET+". ■");
        println("■  Ca ne sera pas une tâche évidente, mais je sais que vous pouvez le faire. "+RED+"Bon courage !!"+RESET+"         ■");
        println("■                                                                                                   ■");
        println("■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■");
    }
    ///////////////
    //DEPLACEMENT//
    ///////////////
    //Récupère un input valide ZQSD
    char deplacementValide(Plateau plateau){
        //Déclaration des variables
        String input = "xx";
        boolean ok = false;
        //Récupération de l'input
        while(ok==false){
            input = majuscule(readString());
            refresh();
            plateauToString(plateau);
            zoneVide();
            if(length(input)==1 && equals(input,"Z") || equals(input,"Q") || equals(input,"S") || equals(input,"D")){
                ok = true;
            }
        }
        return charAt(input,0);
    }
    //Récupère les coordonnés du joueur
    int[] chercheJoueur(Plateau plateau, int[] positionJoueur){
        for(int i=0; i<length(plateau.cases,1); i++){
            for(int u=0; u<length(plateau.cases,2); u++){
                if(plateau.cases[i][u].joueur==true){
                    positionJoueur[0] = i;
                    positionJoueur[1] = u;
                } 
            }
        }
        return positionJoueur;   
    }
    //Vérifie si le joueur n'a pas commencer un combat en se positionnant sur un monstre
    void rechercheCombat(Plateau plateau){
        //Déclaration des variables
        boolean fini = false;
        char input = 'x';
        //Recherche d'un combat
        while(fini==false){
            fini = false;
            //Affiche le plateau
            refresh();
            plateauToString(plateau);
            zoneVide(); //Affiche la zone de texte vide
            //Actualise le tableau en fonction des déplacements
            input = deplacementValide(plateau);
            plateau = nouveauPlateau(plateau, input);
            //Cherche la position du joueur
            int[] positionJoueur = new int[2];
            positionJoueur = chercheJoueur(plateau, positionJoueur);
            //Démarre le combat si le joueur et sur un monstre
            if(plateau.cases[positionJoueur[0]][positionJoueur[1]].joueur==true && plateau.cases[positionJoueur[0]][positionJoueur[1]].monstres==true){
                fini = true;
            }
        }
    }
    //////////
    //COMBAT//
    //////////
    //Compilation des différents programme du combat
    boolean combat(int nbrMonstresVaincus){
        //Déclaration des variables
        CSVFile question = loadCSV(cheminQuestion);
        Personnage p = new Personnage();
        p.stats = new Stats();
        p = donneeJoueur(p);
        boolean fini = false;
        char input = 'x';
        int nbrReponses = 4;
        String[] bonneReponse = new String[2];
        int pvMonstre = p.stats.niveau * 16;
        int degats = 0;
        int i = 1;
        //Début du combat
        while(fini==false){
            //Partie affichage du combats
            refresh();
            ecranCombat();
            infoJoueur(pvMonstre);
            boiteChoixCombat();
            //Analyse l'action demandé par le joueur
            i = 1;
            input = actionValide(i, pvMonstre);
            //Refresh l'écran selon l'action choisis
            refresh();
            ecranCombat();
            infoJoueur(pvMonstre);
            //Le joueur choisis d'attaquer
            if(input=='A' || (input=='B' && p.stats.mana<5)){
                nbrReponses = 4;
                //Récupère la bonne réponse (et affiche la queston et les 4 réponses)
                bonneReponse = question(nbrReponses);
            //Le joueur choisis d'être aider (contre du 5 pts de mana)
            }else if(input=='B'){
                //Enlève pts de mana
                p.stats.mana = p.stats.mana - 5;
                sauvegarde(p,nbrMonstresVaincus);
                //Affiche l'écran d'aide
                refresh();
                ecranCombat();
                infoJoueur(pvMonstre);
                boiteChoixAide();
                //Analyse l'action demandé par le joueur
                i = 2;
                input = actionValide(i, pvMonstre);
                //Refresh l'écran selon l'action choisis
                refresh();
                ecranCombat();
                //Le joueur choisis d'être soignée
                if(input=='A'){
                    nbrReponses = 4;
                    //Soigne le joueur
                    p.stats.pv = p.stats.pvMax;
                    sauvegarde(p,nbrMonstresVaincus);
                    infoJoueur(pvMonstre);
                    //Récupère la bonne réponse (et affiche la queston et les 4 réponses)
                    bonneReponse = question(nbrReponses);
                //Le joueur choisis d'avoir une mauvaise réponse en moins
                }else if(input=='B'){
                    nbrReponses = 3;
                    infoJoueur(pvMonstre);
                    //Récupère la bonne réponse (et affiche la queston et les 3 réponses)
                    bonneReponse = question(nbrReponses);
                }
                
            }
            //Inflige les dégats au joueur ou au monstre
            if(reponseValide(bonneReponse, nbrReponses, pvMonstre)==true){
                degats = (int) (p.stats.niveau * (random()*(5)+5));
                pvMonstre = pvMonstre - degats;
            }else{
                degats = (int) (p.stats.niveau * (random()*(3)+5));
                p.stats.pv = p.stats.pv - degats;
            }
            //Check si le combat n'est pas fini car KO du monstre ou joueur
            if(pvMonstre<1 || p.stats.pv<1){
                fini = true;
            }
            sauvegarde(p,nbrMonstresVaincus);
        }
        /////////////////
        //Fin du combat//
        /////////////////
        return finDuCombat(p, nbrMonstresVaincus);
    }
    //Affiche les PV et le mana du joueur
    void infoJoueur(int pvMonstre){
        //Déclaration des variables
        Personnage p = new Personnage();
        p.stats = new Stats();
        p = donneeJoueur(p);
        //Définition de l'affichage
        String resultat = "■     "+PURPLE+p.prenom+" nv."+p.stats.niveau+GREEN+"  PV: "+p.stats.pv+"/"+p.stats.pvMax+BLUE+"   Mana: "+p.stats.mana+"/"+p.stats.manaMax+RED+"                                     PV Monstre: "+pvMonstre+RESET;
        while(length(resultat)<124){
            resultat = resultat + " ";
        }
        //Affichage
        println(resultat+"■");
    }
    //Affiche les options d'action du joueur
    void boiteChoixCombat(){   
        println("■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■");
        println("■                                                                                                   ■");
        println("■     "+RED+"■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■           "+BLUE+"■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■     "+RESET+"■");
        println("■     "+RED+"■ Appuyez sur \"A\" pour attaquer l'ennemi  ■           "+BLUE+"■ Appuyez sur \"B\" pour être aidé  ■     "+RESET+"■");
        println("■     "+RED+"■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■           "+BLUE+"■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■     "+RESET+"■");
        println("■                                                                                                   ■");
        println("■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■");
    }
    //Affiche les options d'action du joueur
    void boiteChoixAide(){   
        println("■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■");
        println("■                                                                                                   ■");
        println("■     "+RED+"■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■           "+BLUE+"■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■   "+RESET+"■");
        println("■     "+RED+"■ Appuyez sur \"A\" pour vous soigner ■           "+BLUE+"■ Appuyez sur \"B\" pour simplifier le QCM  ■   "+RESET+"■");
        println("■     "+RED+"■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■           "+BLUE+"■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■   "+RESET+"■");
        println("■                                                                                                   ■");
        println("■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■");
    }
    //Vérifie si l'action 'A' ou 'B' est réalisé
    char actionValide(int i, int pvMonstre){
        //Déclaration des variables
        String input = "";
        boolean ok = false;
        //Vérification de la validité de l'action
        while(ok==false){
            input = readString();
            input = majuscule(input);
            refresh();
            ecranCombat();
            infoJoueur(pvMonstre);
            if(i==1){
                boiteChoixCombat();
            }else{
                boiteChoixAide();
            }
            if(length(input)==1 && equals(input,"A") || equals(input,"B")){
                ok = true;
            }
        }
        return charAt(input,0);
    }
    //Fonction qui renvoie la bonne réponse ET affiche la question et les 3 ou 4 réponses possible
    String[] question(int nbrReponses){
        //Déclaration des variables
        CSVFile questions = loadCSV(cheminQuestion);
        int cmpt = 0;
        int i1 = 0; int i2 = 0; int i3 = 0; int i4 = 0;
        int alea = (int) (random()*23);
        String[] retour = new String[2];
        //Récupération de la question et des réponses (3 OU 4)
        String question = getCell(questions,alea,0);
        String[] reponse = new String[nbrReponses];
        for(int i = 0; i<length(reponse); i++){
            reponse[i] = getCell(questions,alea,(i+1));
        }
        //Mise en place de la partie graphique
        String l1 = "■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■";
        String l2 = "■                                                                                                   ■";
        String l3 = "■  ";
        String l4 = "■  ";
        String l5 = "■  ";
        String l6 = "■                                                                                                   ■";
        String l7 = "■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■";
        //SYSTEME DE RANDOMISATION DES REPONSES (PAS TOUJOURS LA MEME REPONSE POUR LA MEME QUESTION)
        //Place correctement la question sur la ligne 3
        l3 = l3 + RED + question + RESET;
        while(length(l3)<109){
            l3 = l3 + " ";
        } l3 = l3 + "■";
        //Place les deux premières questions correctement sur la ligne 4
            //Première réponse
        i1 = (int) (random()*(nbrReponses));
        l4 = l4 + "A - " + reponse[i1];
        while(length(l4)<59){
            l4 = l4 + " ";
        }
            //Deuxième réponse
        do{
            i2 = (int) (random()*(nbrReponses));
        }while(i1==i2);
        l4 = l4 + "B - " + reponse[i2];
        while(length(l4)<100){
            l4 = l4 + " ";
        } l4 = l4 + "■";
        //Place les deux dernières question sur la ligne 5
            //Troisième réponse
        do{
            i3 = (int) (random()*(nbrReponses));
        }while(i1==i3 || i2==i3);
        l5 = l5 + "C - " + reponse[i3];
        while(length(l5)<59){
            l5 = l5 + " ";
        }
        //Quatrième réponse
        if(nbrReponses==4){
            do{
                i4 = (int) (random()*(nbrReponses));
            }while(i1==i4 || i2==i4 || i3==i4);
            l5 = l5 + "D - " + reponse[i4];
        }
        while(length(l5)<100){
                l5 = l5 + " ";
        } l5 = l5 + "■";
        //Affichage finale
        retour[0] = l1 + "\n" + l2 + "\n" + l3 + "\n" + l4 + "\n" + l5 + "\n" + l6 + "\n" + l7;
        //Retourne la bonne réponse
        if(i1==0){
            retour[1] = "A";
        }else if(i2==0){
            retour[1] = "B";
        }else if(i3==0){
            retour[1] = "C";
        }else{
            retour[1] = "D";
        }
        println(retour[0]);
        return retour;
    }
    //Vérifie si la réponse du joueur est la bonne
    boolean reponseValide(String[] bonneReponse, int nbrReponses, int pvMonstre){
        //Déclaration des variables
        String input = "";
        boolean ok = false;
        //Vérifie la validité de la réponse
        while(ok==false){
            input = majuscule(readString());
            refresh();
            ecranCombat();
            infoJoueur(pvMonstre);
            println(bonneReponse[0]);
            if(length(input)==1 && (equals(input,"A") || equals(input,"B") || equals(input,"C") || (equals(input,"D") && nbrReponses==4))){
                ok = true;
            }
        }
        return equals(input,bonneReponse[1]);
    }
    //Vérifie l'issue du combat DEFAITE ou VICTOIRE
    void testFinDuCombat(){
        //Déclaration des variables
        Stats stats = new Stats();
        Personnage p = new Personnage();
        p = donneeJoueur(p);
        //FALSE = DEFAITE  PV Joueur < 0 donc DEFAITE
        p.stats.pv = 0;
        assertFalse(finDuCombat(p, 0));
        //TRUE = VICTOIRE  PV Joueur > 0 donc VICTOIRE
        p.stats.pv = (int) (random()*100+1);;
        assertTrue(finDuCombat(p, 0));
    }
    boolean finDuCombat(Personnage p, int nbrMonstresVaincus){
        //Déclaration des variables
        Stats stats = new Stats();
        int pvSupp = 0;
        //Vérificatiopn du résultat de la fin du combat (Victoire ou Défaite)
        if(p.stats.pv>0){
            //Level up
            p = donneeJoueur(p);
            p.stats.niveau = p.stats.niveau + 1;
            pvSupp = (int) ((random()*5)+11);
            p.stats.pvMax = p.stats.pvMax + pvSupp;
            //Soigne le joueur
            p.stats.pv = p.stats.pvMax; 
            p.stats.mana = p.stats.manaMax;
            sauvegarde(p,nbrMonstresVaincus);
            //Victoire
            return true;
        }else{ 
            //Défaite
            return false;
        }
    }
    ////////////////////////////
	//COMPILATION DU PROGRAMME//
    ////////////////////////////
    void algorithm(){
        //Déclaration des variables
        CSVFile sauvegarde = loadCSV(chemin);
        Personnage p = new Personnage();
        Plateau plateau = new Plateau();
        p.stats = new Stats();
        boolean fini = false;
        char input = 'x';
        /////////////////////
        //Menu de démarrage//
        /////////////////////
        accueil();
        p = donneeJoueur(p);
        int nbrMonstresVaincus = donneeMonstre();
        int nbrMonstres = 5;
        ////////////////////////
        //Début du jeu / pts 0//
        ////////////////////////
        if(p.sauvegarde==0){
            introduction();
            p = donneeJoueur(p);
            p.sauvegarde = 1;
            sauvegarde(p,0);
        }
        //Soigne le joueur
        p.stats.pv = p.stats.pvMax; p.stats.mana = p.stats.manaMax; //Soigne le joueur
        sauvegarde(p,nbrMonstresVaincus);
        /////////////////////////////
        //Phase de tutoriel / pts 1//
        /////////////////////////////
        if(p.sauvegarde==1){
            refresh();
            //Génération d'un plateau aléatoire avec un monstre
            plateau = generationPlateau(25, 101, 1);
            //Début du tutoriel
            tutoriel1(plateau, nbrMonstresVaincus);
            p = donneeJoueur(p);
            p.sauvegarde = 2;
            sauvegarde(p,nbrMonstresVaincus);
            //Fin du tutoriel
            tutoriel2(plateau);
        }
        //////////////////////////////////////////////
        //Phase d'éradication des 5 monstres / pts 2//
        //////////////////////////////////////////////
        if(p.sauvegarde==2 && donneeMonstre()!=5){
            refresh();
            //Initialise un nouveau plateau avec le nombre de monstre adéquat
            while(fini==false){
            //Initialisation du Jeu partie 2
                //Soigne le joueur
                p = donneeJoueur(p);
                p.stats.pv = p.stats.pvMax; 
                p.stats.mana = p.stats.manaMax;
                sauvegarde(p,nbrMonstresVaincus);
            //Génération d'un plateau aléatoire avec 5 monstres de bases
                nbrMonstres = 5 - nbrMonstresVaincus;
                plateau = generationPlateau(25, 101, nbrMonstres);
            //Début du jeu partie 2
                //Recherche un combat avec un monstre
                rechercheCombat(plateau);
                //Débute le combat
                if(combat(nbrMonstresVaincus)==false){
                    perdu();
                }else{
                    nbrMonstresVaincus = nbrMonstresVaincus + 1;
                    p = donneeJoueur(p);
                    sauvegarde(p,nbrMonstresVaincus);
                }
            //Vérifie si les monstres sont tous vaincus
                nbrMonstres = 5 - donneeMonstre();
                fini = finDuJeu(nbrMonstres);
            }
        }
        //FIN DU JEU
        refresh();
        fin();
        readString();
        algorithm(); //retour au début du jeu
    }
}