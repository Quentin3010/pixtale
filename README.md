# PIXTALE

Quentin BERNARD

## Description du projet

PIXTALE est un projet d'informatique réalisé durant le premier semestre de ma première année de DUT Informatique. Il s'agit de mon tout premier projet dans le cadre de mes études, et il a été développé en utilisant le langage iJava, qui nous a été imposé pour cette occasion.

iJava est un langage de programmation qui repose sur une combinaison de la syntaxe Java et de fonctionnalités spécifiques à l'enseignement de l'informatique. Il a été conçu pour faciliter l'apprentissage des concepts fondamentaux de la programmation tout en offrant un environnement convivial et adapté aux débutants.

Le principal objectif de notre projet PIXTALE était de consolider nos connaissances acquises au cours du premier semestre. Pour cela, nous avons été confrontés à un défi stimulant : concevoir et développer un jeu ludo-éducatif destiné aux jeunes enfants.

Nous avons choisi de créer un jeu de type tour par tour, dans lequel les enfants peuvent tester leurs connaissances générales à travers des QCM (Questionnaires à Choix Multiples) et d'autres activités éducatives. L'idée était d'allier divertissement et apprentissage, en rendant le processus d'acquisition de connaissances ludique et interactif.

Vidéo de présentation : https://youtu.be/hH9In4OY2Ec

## Fonctionnalités

1 - Map :
Génération aléatoire d'une carte sur laquelle le joueur peut se déplacer librement.
Limites de la carte et obstacles pour empêcher le joueur de sortir ou de passer à travers les obstacles.

2 - Système de combat :
Deux choix pour le joueur : Attaquer ou être aidé.
Attaque : Le joueur peut infliger des dégâts aux monstres adverses en répondant correctement aux questions du QCM.
Si le joueur répond incorrectement, il subit des dégâts.
Être aidé : Le joueur peut choisir de se soigner intégralement en échange de 5 points de mana ou d'éliminer une mauvaise réponse du QCM.

3 - QCM :
QCM composés de questions variées : Mathématiques, Français, EMC, Géographie, Sport, etc.
Chaque question propose initialement 4 réponses possibles, dont une seule est correcte.
L'option d'éliminer une mauvaise réponse réduit les choix à 3 réponses, dont une seule est correcte.
La victoire du joueur dépend de la réduction des points de vie du monstre à zéro, tandis que sa défaite survient lorsque ses propres points de vie atteignent zéro.

4 - Statistiques :
Le joueur possède des statistiques telles que le nom, les points de vie (PV), les points de vie maximaux, la mana, la mana maximale et le niveau.
Le joueur gagne un niveau à chaque victoire, ce qui augmente ses points de vie (aléatoirement).
La puissance d'attaque du joueur dépend de son niveau et d'un système d'aléatoire pour les dégâts.
Les points de vie des monstres sont déterminés en fonction du niveau du joueur.

5 - Sauvegarde :
Des points de sauvegarde sont disséminés dans le jeu pour permettre au joueur de sauvegarder sa progression.
La sauvegarde permet au joueur de reprendre là où il a laissé sa partie lorsqu'il quitte le jeu.
